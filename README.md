* apksigner 
wrapper pour apksigner.jar
source :  https://github.com/patrickfav/uber-apk-signer/releases

* apktool
wrapper pour apktool.jar
source : https://bitbucket.org/iBotPeaches/apktool/downloads

* baksmali
wrapper pour baksmali.jar
source : https://bitbucket.org/JesusFreke/smali/downloads

* getSignature
wrapper pour getSignature.jar

* smali
wrapper pour smali.jar
source : https://bitbucket.org/JesusFreke/smali/downloads

## XPosed

* Riru-Core Module(Magisk
source : https://github.com/RikkaApps/Riru/releases/latest 

* YAHFA Module(Magisk)
source : https://github.com/ElderDrivers/EdXposed/releases/latest

* EdXposedManager
source : https://github.com/ElderDrivers/EdXposedManager/releases/latest
 
